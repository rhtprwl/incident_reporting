require "application_system_test_case"

class IncidentsTest < ApplicationSystemTestCase
  setup do
    @incident = incidents(:one)
  end

  test "visiting the index" do
    visit incidents_url
    assert_selector "h1", text: "Incidents"
  end

  test "creating a Incident" do
    visit incidents_url
    click_on "New Incident"

    fill_in "Description", with: @incident.description
    fill_in "Email", with: @incident.email
    fill_in "Incident Datetime", with: @incident.incident_datetime
    fill_in "Incident Type", with: @incident.incident_type
    fill_in "Location", with: @incident.location
    fill_in "Name", with: @incident.name
    fill_in "Other Observers", with: @incident.other_observers
    fill_in "People Involved", with: @incident.people_involved
    fill_in "Phone", with: @incident.phone
    fill_in "Role", with: @incident.role
    fill_in "School", with: @incident.school
    click_on "Create Incident"

    assert_text "Incident was successfully created"
    click_on "Back"
  end

  test "updating a Incident" do
    visit incidents_url
    click_on "Edit", match: :first

    fill_in "Description", with: @incident.description
    fill_in "Email", with: @incident.email
    fill_in "Incident Datetime", with: @incident.incident_datetime
    fill_in "Incident Type", with: @incident.incident_type
    fill_in "Location", with: @incident.location
    fill_in "Name", with: @incident.name
    fill_in "Other Observers", with: @incident.other_observers
    fill_in "People Involved", with: @incident.people_involved
    fill_in "Phone", with: @incident.phone
    fill_in "Role", with: @incident.role
    fill_in "School", with: @incident.school
    click_on "Update Incident"

    assert_text "Incident was successfully updated"
    click_on "Back"
  end

  test "destroying a Incident" do
    visit incidents_url
    page.accept_confirm do
      click_on "Destroy", match: :first
    end

    assert_text "Incident was successfully destroyed"
  end
end
