class IncidentsController < ApplicationController
  before_action :set_incident, only: [:simple_report, :detailed_report]

  # POST /
  def simple_report_create
    create_report :simple_report
  end


  # POST /incidents/detailed_report
  def detailed_report_create
    create_report :detailed_report
  end

  # GET /incidents/thank_you
  def thank_you
    redirect_to root_path unless request.referrer.present?
  end

  private
    def set_incident
      @incident = Incident.new
      @incident.incident_files.build
    end

    def create_report source
      @incident = Incident.new(incident_params)

      if @incident.save
        redirect_to thank_you_incidents_path
      else
        render source
      end
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def incident_params
      params.require(:incident).permit(:school, :description, :incident_type, :name, :email, :phone, :role, :people_involved, :other_observers, :incident_datetime, :location, incident_files_attributes: [:id, :attachment, :_destroy])
    end
end
