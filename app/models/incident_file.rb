class IncidentFile < ApplicationRecord
  has_attached_file :attachment
  
  # associations
  belongs_to :incident

  # validations
  do_not_validate_attachment_file_type :attachment # allow all types files as attachment
end
