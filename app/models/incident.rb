class Incident < ApplicationRecord

  enum incident_type: %w[Alcohol Assault Bullying Hazing Drugs Graffiti Injury Suspicious\ Activity Trespassing Vandalism Weapons\ Violation Technology\ Misuse Other]
  enum role: %w[Student Teacher Other\ Staff Parent Community\ Member]

  # associations
  has_many :incident_files, dependent: :destroy

  # validations
  validates :description, presence: true

  accepts_nested_attributes_for :incident_files
end
