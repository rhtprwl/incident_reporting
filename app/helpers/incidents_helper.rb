module IncidentsHelper
  def predefined_locations
    [
      'On school campuses and grounds',
      'During transportation to and from school', 
      'At school-sponsored events',
      'More details'
    ]
  end

  def get_collection enum_attribute
    enum_attribute.keys.map { |key| [key.humanize, key] }
  end
end
