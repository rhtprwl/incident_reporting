class CreateIncidents < ActiveRecord::Migration[5.2]
  def change
    create_table :incidents do |t|
      t.string :school
      t.text :description
      t.integer :incident_type, default: 0
      t.string :name
      t.string :email
      t.string :phone
      t.integer :role, default: 0
      t.text :people_involved
      t.text :other_observers
      t.datetime :incident_datetime
      t.string :location

      t.timestamps
    end
  end
end
