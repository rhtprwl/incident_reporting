Rails.application.routes.draw do
  resources :incidents do
    collection do
      get :detailed_report
      post 'detailed_report' => 'incidents#detailed_report_create'
      get :thank_you
    end
  end
  root 'incidents#simple_report'
  post '/' => 'incidents#simple_report_create'
end
